SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )
export HOST_ROOT="${SCRIPT_DIR}/.."
export MODULES_DIR="${HOST_ROOT}"/modules
export GUEST_ROOT=/srv/docker-dev

# FIXME: Streamline the static variable.  Move to "use" script?
if [ -v 'LOADED_MODULES_IS_SET_UP' ]; then
	declare -A LOADED_MODULES=()
	export LOADED_MODULES_IS_SET_UP=1
fi

function module-up {
    module="$1"

    if [[ ${!LOADED_MODULES[@]} =~ "${module}" ]]; then
        echo "Skipping module '$module' because it was already loaded"
        return
    fi
    LOADED_MODULES+=("$module" 1)

    echo "Installing module '$module'"
    "${MODULES_DIR}"/${module}/bin/up
}

function launch-docker {
    docker_compose_config="$1"

    docker compose --file "$docker_compose_config" up --build --detach
    # TODO: Show logs
}

function wait-for {
    while ! $1
    do
        sleep 3
    done
}
