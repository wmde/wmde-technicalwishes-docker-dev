<?php

# https://www.mediawiki.org/wiki/Extension:CentralAuth
# https://www.mediawiki.org/wiki/Manual:$wgConf#Example

wfLoadExtension( 'AntiSpoof' );
wfLoadExtension( 'CentralAuth' );

$wgCentralAuthDatabase = 'centralauth';
$wgCentralAuthAutoMigrate = true;
$wgCentralAuthAutoMigrateNonGlobalAccounts = true;

// FIXME: enumerate all existing sites
$wgLocalDatabases = array(
    'dev',
    'centralauth',
	'commons'
);

$wgConf->wikis = $wgLocalDatabases;
$wgConf->localVHosts = array( 'database' );

$port = getenv( 'MW_DOCKER_PORT' );

$wgConf->settings = array(
    'wgServer' => array(
        'default' => "http://dev.wiki.local.wmftest.net:{$port}",
        'centralauth' => "http://centralauth.wiki.local.wmftest.net:{$port}",
        'commons' => "http://commons.wiki.local.wmftest.net:{$port}",
    ),

    'wgCanonicalServer' => array(
        'default' => "http://dev.wiki.local.wmftest.net:{$port}",
        'centralauth' => "http://centralauth.wiki.local.wmftest.net:{$port}",
        'commons' => "http://commons.wiki.local.wmftest.net:{$port}",
    ),

    'wgScriptPath' => array(
        'default' => '/w',
    ),

    'wgArticlePath' => array(
        'default' => '/wiki/$1',
    ),

    'wgSitename' => array(
        'dev' => 'default',
        'centralauth' => 'centralauth',
        'commons' => 'commons',
    ),
);

$wgConf->suffixes = $wgLocalDatabases;
$wgConf->extractAllGlobals( $wgDBname );

$wgCentralAuthCookies = true;
# Create the local account on pageview, set false to require a local login to create it.
$wgCentralAuthCreateOnView = true;

# Activates the redirect to the "central login wiki"
// $wgCentralAuthLoginWiki = 'centralauth';

# Skips the "login success" page
// $wgCentralAuthSilentLogin = true;

$wgDebugLogGroups[ 'CentralAuth' ] = '/srv/log/CentralAuth.log';
