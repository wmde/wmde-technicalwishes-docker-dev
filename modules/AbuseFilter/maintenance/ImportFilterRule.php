<?php

use MediaWiki\Extension\AbuseFilter\AbuseFilterServices;
use MediaWiki\Extension\AbuseFilter\Filter\MutableFilter;
use MediaWiki\Extension\AbuseFilter\FilterImporter;
use MediaWiki\Extension\AbuseFilter\FilterStore;
use MediaWiki\MediaWikiServices;

$IP = getenv( 'MW_INSTALL_PATH' );
if ( $IP === false ) {
	$IP = __DIR__ . '/../../..';
}
require_once "$IP/maintenance/Maintenance.php";

class ImportFilterRule extends Maintenance {
	/**
	 * @inheritDoc
	 */
	public function __construct() {
		parent::__construct();

		$this->addOption( 'rule', 'JSON-formatted string of a rule to import', true, true, false, true );
		$this->requireExtension( 'Abuse Filter' );
	}

	public function execute() {
		$services = MediaWikiServices::getInstance();
		$filterImporter = AbuseFilterServices::getFilterImporter();
		$filterStore = AbuseFilterServices::getFilterStore();
		$adminUser = $services->getUserFactory()->newFromName( 'Admin' );

		foreach ( $this->getOption( 'rule' ) as $ruleEncoded ) {
			$filter = $filterImporter->decodeData( $ruleEncoded );
			$status = $filterStore->saveFilter( $adminUser, null, $filter, MutableFilter::newDefault() );
			if ( !$status->isGood() ) {
				$this->fatalError( $status->getMessage() );
			}
		}
	}
}

$maintClass = ImportFilterRule::class;
require_once RUN_MAINTENANCE_IF_MAIN;
