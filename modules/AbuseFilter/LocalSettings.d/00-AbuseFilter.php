<?php

# https://www.mediawiki.org/wiki/Extension:AbuseFilter

wfLoadExtension( 'AbuseFilter' );

// The following sample configuration allows sysops to do everything they want
// with AbuseFilter, and everyone to view the log and see public filter
// settings:
$wgGroupPermissions['sysop']['abusefilter-modify'] = true;
$wgGroupPermissions['*']['abusefilter-log-detail'] = true;
$wgGroupPermissions['*']['abusefilter-view'] = true;
$wgGroupPermissions['*']['abusefilter-log'] = true;
$wgGroupPermissions['sysop']['abusefilter-privatedetails'] = true;
$wgGroupPermissions['sysop']['abusefilter-modify-restricted'] = true;
$wgGroupPermissions['sysop']['abusefilter-revert'] = true;
