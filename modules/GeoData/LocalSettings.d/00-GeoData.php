<?php

wfLoadExtension( 'GeoData' );

$wgGeoDataUseCirrusSearch = true;
$wgGeoDataBackend = 'elastic';

$wgMaxGeoSearchRadius = 1000000;
$wgDebugLogGroups[ 'CirrusSearchRequests' ] = '/srv/log/CirrusSearchRequests.log';
