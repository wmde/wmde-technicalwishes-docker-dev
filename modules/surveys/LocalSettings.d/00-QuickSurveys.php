<?php

wfLoadExtension( 'QuickSurveys' );

$wgQuickSurveysConfig = [
    [
        'name' => 'external example survey',
        // Internal or external link survey
        'type' => 'external',
        // Survey question message key
        'question' => 'ext-quicksurveys-example-external-survey-question',
        // The i18n key of the description of the survey
        'description' => 'ext-quicksurveys-example-external-survey-description',
        // External link to the survey
        'link' => 'ext-quicksurveys-example-external-survey-link',
        // Parameter to add to external link
        //'instanceTokenParameterName' => 'parameterName',
        'enabled' => true,
        // Percentage of users that will see the survey
        'coverage' => 1,
        // For each platform (desktop, mobile), which version of it is targeted
        'platforms' => [
            'desktop' => [ 'stable' ],
            'mobile' => [ 'stable' ]
        ],
        'audience' => [
            'anons' => true,
        ],
        // The i18n key of the privacy policy text
        'privacyPolicy' => 'ext-quicksurveys-example-external-survey-privacy-policy',
    ],
];
