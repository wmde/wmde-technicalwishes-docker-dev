#!/bin/bash

set -euxo pipefail

MODULE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"/..
SCRIPT_DIR="${MODULE_DIR}"/../../bin
source "${SCRIPT_DIR}"/functions.sh

clone-extension CirrusSearch
clone-extension Elastica

link-module-localsettings elastic

launch-docker "${MODULE_DIR}"/docker/docker-compose.yml

# Installs the dependencies for Elastica
composer-install extensions/Elastica
composer-install extensions/CirrusSearch

function probe-elastic { curl -s http://localhost:9200/_cluster/health | grep -q green; }
wait-for probe-elastic

# Configure the search index and populate it with content
mwscript extensions/CirrusSearch/maintenance/UpdateSearchIndexConfig.php --wiki=dev
mwscript extensions/CirrusSearch/maintenance/ForceSearchIndex.php --wiki=dev --skipLinks --indexOnSkip
mwscript extensions/CirrusSearch/maintenance/ForceSearchIndex.php --wiki=dev --skipParse
# Process the job queue. You need to do this any time you add/update content and want it updated in ElasticSearch
mwscript runJobs.php --wiki=dev --quiet
# Create the suggester index (autocomplete). It needs to be refreshed manually after adding/deleting new pages.
mwscript extensions/CirrusSearch/maintenance/UpdateSuggesterIndex.php --wiki=dev
