<?php

# https://www.mediawiki.org/wiki/Extension:FileImporter

if ( $wgDBname === 'commons' || defined( 'MW_PHPUNIT_TEST' ) ) {
	wfLoadExtension( 'FileImporter' );
	$wgFileImporterShowInputScreen = true;

	// TODO limit source sites when using multiwikis and SUL is working
	$wgFileImporterSourceSiteServices = [
		//	'default' => 'FileImporter-WikimediaSitesTableSite',
	];

	$wgFileImporterCommonsHelperServer = 'http://commons.wiki.local.wmftest.net:' . getenv( 'MW_DOCKER_PORT' );
	$wgFileImporterCommonsHelperBasePageName = 'Extension:FileImporter/Data/';
	$wgFileImporterCommonsHelperHelpPage = 'http://commons.wiki.local.wmftest.net:' . getenv( 'MW_DOCKER_PORT' ) . '/wiki/Extension:FileImporter/Data';

	// Note that we're relying on the Beta Cluster
	$wgFileImporterWikidataEntityEndpoint = 'https://wikidata.beta.wmflabs.org/wiki/Special:EntityData/';
	$wgFileImporterWikidataNowCommonsEntity = 'Q531650';

	// TODO enable when SUL is working
	$wgFileImporterSourceWikiTemplating = false;
	$wgFileImporterSourceWikiDeletion = false;

	$wgHTTPConnectTimeout = 20;
}

$wgDebugLogGroups[ 'FileImporter' ] = '/srv/log/FileImporter.log';

$wgFileImporterCodexMode = true;
